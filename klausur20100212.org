Lösung Klausuer 2010 02 12

* Der Unviversalrechenautomat

** 

- Befehlsholphase :: Der nächste zu bearbetende Befehl wir aus dem Speicher geladen
- Dekodierphase :: Die Steuersignale die für den Befehl notwendig sind werden generiert
- Operandenholphase :: Die Operanden werden an die ALU angelegt
- Ausführphase :: Die ALU berechnet das Ergebniss des Befehls 
- Rückschreibphase :: Das Ergebnis wird in Zielregister oder die Zielspeicherstelle geschrieben
- Adressierungsphase :: Die Adresse des nächsten Befehls wird in den Befehlzähler gelegt

** 

Der Befehl wird (sofern möglich) bis zum Ende ausgeführt, worauf dann die Adresse an der der Interupt oder die Exception behandelt wird in den Befehlszähler geschrieben wird.

* Befehlssatzarchitekturen

** Stack

#+BEGIN_EXAMPLE 
push y
push z
sub
pop x
#+END_EXAMPLE

** Akkumulator

#+BEGIN_EXAMPLE 
load y
sub z
store x
#+END_EXAMPLE

** Register-Memory

#+BEGIN_EXAMPLE
.long y
.long z
.long x
...
movl y , %eax
subl z , %eax
movl %eax , x
#+END_EXAMPLE

** Register-Register

#+BEGIN_EXAMPLE 
load y , %R1
load z , %R2
sub %R1 , %R2 , %R3
store %R3 , x
#+END_EXAMPLE

* Speicher

** 

- Litte-Endian :: An den niedrigeren Adressen stehen die niederwertigeren Bytes
- Big-Endian :: Umgekehrt

** 

|    | 0x0001 (1) | 0x0100 (256) |
|----+------------+--------------|
| LE | 0x01 0x00  | 0x00 0x01    |
| BE | 0x00 0x01  | 0x01 0x00    |

** 

Ein Datum des Typs $x$ steht an der eine Adresse $A$ die die Eigenschaft $A\b\mod{\rm sizeof}(x)=0$ erfüllt

** 

#+BEGIN_EXAMPLE  
bb|bb|bb|pp|bb bb|bp|pp

b steht für ein benutztes Byte, p für Padding das für das Alignment benötigt wird
#+END_EXAMPLE

$10\cdot 16=160$

* CPU-Architekturen

** 

Die Funktionalen Einheiten der CPU (ALU, Befehlsdekodierung) werden, anstatt nur einmal im Zyklus innerhalb ihrer Phase genutzt zu werden, parralel auf Folgebefehle verwendet


** 

Auf der Die eines Prozessors werden mehrere CPUs integriert, die aber weitgehend unabhängig voneinenander arbeiten.

** 

Vektor-Operationen werden parallel über duplizierte ALUs ausgeführt

* Busse

** 

- Adress-Leitungen :: Über diese Leitungen wird die Adresse eines zu ladenden/schreibenden Datums weitergegeben
- Daten-Leitungen :: Durch diese Leitungen wird das Datum weitergegeben
- Kontroll-Leitungen :: Zustandssignale sowie Schreib/Lesebefehle werden mit diesen Leitungen übermittelt

** 

   $$4 \cdot 2^{30} = 4294967296\approx 4GByte$$

* Ausdrücke

#+BEGIN_EXAMPLE 
;; laden der Operanden

movl a , %eax
movl b , %eax

;; mantissen isolieren
;; V EEEEEEEEEEEEEEE MMMMMMMMMMMMMMMM  bitwise and
;; 0 000000000000000 1111111111111111  = 0x 0000 ffff
;; ----------------------------------
;; 0 000000000000000 MMMMMMMMMMMMMMMM

andl $0xffff , %eax
andl $0xffff , %ebx

;; Mantissen multiplzieren

imull %ebx , %eax

;; an die richtige Stelle schieben

shr $16 , %eax

;; zwischenspeichern

movl %eax , %edx

;; Operanden nochmal laden

movl a , %eax
movl b , %eax

;; Exponenten isolieren
;; V EEEEEEEEEEEEEEE MMMMMMMMMMMMMMMM  bitwise and
;; 0 111111111111111 0000000000000000  = 0x 7fff 0000
;; ----------------------------------
;; 0 EEEEEEEEEEEEEEE 0000000000000000  shift right 16
;; 0 000000000000000 0EEEEEEEEEEEEEEE


andl $0x7fff0000 , %eax
shrl $16 , %eax
andl $0x7fff0000 , %ebx
shrl $16 , %ebx

;; Exponenten multiplizieren

imull %ebx , %eax

;; zurückschrieben

shll $16 , %eax

;; in das zwischenergebniss schreiben

orl %eax , %edx

;; nochmal Operanden laden

movl a , %eax
movl b , %eax

;; Vorzeichen berechenen

xorl %ebx , %eax

;; Vorzeichen isolieren
;; V EEEEEEEEEEEEEEE MMMMMMMMMMMMMMMM  bitwise and
;; 1 000000000000000 0000000000000000  = 0x 8000 0000
;; ----------------------------------
;; V 000000000000000 0000000000000000

andl $0x80000000 , %eax

;; mit dem Zwischenergebniss kombinieren

orl %edx , %eax
#+END_EXAMPLE

* Zwei-Adress-Code

#+BEGIN_EXAMPLE
unsigned int val = 0;
char c = fgetc(fp);
char c_tmp1 = c;
char c_tmp2 = c;
c_tmp1 = c_tmp1 < '0';
c_tmp2 = c_tmp2 > '9';
c_tmp1 = c_tmp1 & c_tmp2;
.while_begin:
if (c_tmp1)  goto .while_end;
val = val * 10;
val = val + c;
val = val - '0';
c = fgetc(fp);
c_tmp1 = c;
c_tmp2 = c;
c_tmp1 = c_tmp1 < '0';
c_tmp2 = c_tmp2 > '9';
c_tmp1 = c_tmp1 & c_tmp2;
goto .while_begin;
.while_end:
#+END_EXAMPLE

* Protection

- In den Speicheradressen 0x0000 0000 -- 0x0000 07ff befindet sich das Page Table Directory
- Das Read-Only-Bit ist in keinem Eintrag gesetzt (per Konvention, wie die Read-Only-Bits auf den verschieden Stufen behandelt werden ist architekturabhängig).
- Das Preset-Bit ist nur im ersten Eintrag gesetzt
- Nur der erste Eintrag ist nötig, da nur eine einzige Pagetable vorhanden ist: die ersten 10 Bit ($\log_2 1024 = 10$) sind für die Auswahl innerhalb der Page Table Directory zuständig. Die höchste Anzusprechende Adresse lautet 0x003fffff = 0000 0000 00 | 11 1111 1111 ...; das alle niedrigeren Adressen mit den gleichen 10 Bit anfangen ist trival ersichtlich
- Dieser Eintrag verweist auf die Adresse 0x0000 0800 also sind die Bits 31-12: 0x0000 8
- In dieser Page Table werden die nächsten 10 Bits verwendet um innerhalb der Pagetable einen Eintrag auszuwählen
- Für den ROM-Speicher benötigt man 2 Pages: Beginn ist 0x00001000 = 0000 0000 00 | 00 0000 0001 | 0000 ...; Ende ist 0x00002fff = 0000 0000 00 | 00 0000 0010 | 1111 ...
- Im Eintrag am Index 1 steht also: 0x0fffe; am Index 2: 0x0ffff; das Read-Only sowie das Present-Bit sind gesetzt
- Das Daten-Bereich benötigt 14 Einträge : Begin ist 0x003f0000 = 0000 0000 00 | 11 1111 0000 | 0000 ...; Ende ist 0x003fefff = 0000 0000 00 | 11 1111 1110 | 1111 ...;
- Die Einträge befinden an den Indizes 0x3f0 -- 0x3fe und verweisen mit den Einträgen 0x0000 9 -- 0x0001 7 auf den Speicherbereich dirkt nach dieser Page
- Der Stack-Bereich benötigt nur eine Seite: Begin ist  0x003ff000 = 0000 0000 00 | 11 1111 1111 | 0000 ...; Ende ist 0x003fffff = 0000 0000 00 | 11 1111 1111 | 1111 ...;
- Der Eintrag für den Stack verweist auf Bereich nach dem Daten-Bereich: 0x0001 8
- In den Einträgen für den Stack und die Daten sind jeweils das Present-Bit gesetzt, das Read-Only-Bit nicht.
- Alle Anderen Einträge haben kein gesetztes Present-Bit

* Caching
  
** 

#+BEGIN_EXAMPLE 
;; Die Syntax enspricht der auf irgendeinem Foliensatz, der eh nur aus Henessy & Patterson kopiert wurde entnommen
movl $0 , %eax ; Regs[eax] = 0
movl $1 , 2000 ; Mem[2000 ] = 1
movl $1 , 2004 ; Mem[2004 ] = 1
movl $2000 , %ebx ; Regs[ebx] = 2000
loop:
movl (%ebx) , %ecx ; Regs[ecx] = Mem[Regs[ebx]]
addl 4(%ebx) , %ecx ; Regs[ecx] = Regs[ecx] + Mem[4 + Regs[ebx]]
movl %ecx , 8(%ebx) ; Mem[8 + Regs[ebx]] = Regs[ecx]
addl $4 , %ebx ; Regs[ebx] = Regs[ebx] + 4
addl $1 , %eax ; Regs[eax] = Regs[eax] + 1
cmpl $11 , %eax ; Flags werden entsprechend dem vergleich von 11 und eax gesetzt
jne loop ; wenn eax nicht 11 ist springe zu loop
;; Die schleife läuft 11 mal
#+END_EXAMPLE

** 

2000 -- 2052

** 

$((100+1)\cdot4+100\cdot2)+((100+1)\cdot7+3\cdot100)\cdot11 = 11681$

** 

$((100+1)\cdot4+100\cdot2)+100\cdot7+((100+7\cdot1)\cdot11) = 2481$
